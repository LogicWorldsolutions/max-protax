<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '');

/** MySQL database username */
define('DB_USER', '');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$[{9Umf]A*Il: wU{F*$|{2_Ty9otrTHv?i>yAM/s$IO{?1WDGb19{=v6O]|V=-R');
define('SECURE_AUTH_KEY',  'k(DzNWm3ntaMiN<|6H57BH#tz#2Kw)b&BG~:R G8g)h0E<wW-tY[*R^Avm,s_zC!');
define('LOGGED_IN_KEY',    '2o4Ry !ZB_?{TEyt!.!FSQU,Sx3IA.D`u)o1,5-]Vy;nPj^O,IMD&] h#fWj%M??');
define('NONCE_KEY',        '}>LjiiKeMPxOp-*Z#I.}5)W3b|p`M4ZFx=U3yp&9a7$)xx%XJ|4q+<,Q]/^;+8S>');
define('AUTH_SALT',        't 3_NDW98`9[8@=eS %?gVPw/cu!C1uEjxu_:/7]t~BGqZp67_*?Hx:lGGe4D6cC');
define('SECURE_AUTH_SALT', 'W}6@e*OGJt<=WY/tdP6y^y+VO0oM0CDeW,X0!G6}eQ+}YeI]|(Bda;-+{1eS_&c-');
define('LOGGED_IN_SALT',   '`c>e<Xc4FmOsb2#.RtnbmZR0HJ^[SfyRGg&@AaQ|Eu<p2`b;S>RIm!!_kXRFA-P&');
define('NONCE_SALT',       ')X,w1?Wy6K^4)xt.=CNj}9}59Kci$v.l6=9RO69FKf!u+t vcV[=-@mdd#~uZX;T');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'css_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);  

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
